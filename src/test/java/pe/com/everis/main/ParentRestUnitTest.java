package pe.com.everis.main;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.everis.main.models.entity.Parent;
import pe.com.everis.main.restcontroller.ParentRest;
import pe.com.everis.main.services.implement.ParentImplement;


@RunWith(SpringRunner.class)
@WebMvcTest(ParentRest.class)
class ParentRestUnitTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ParentImplement parentService;
	
	private Parent  parentTest=new Parent();
	
	@Before
    public void init(){
      parentTest = new Parent(5L, "M", "Rosa", "Maria", "Savedra", "Parent");
    }
	
	@Test
	public void testGetAllParentsSucces() throws Exception {
		List<Parent> parents = Arrays.asList(parentTest);
	    when(parentService.findAll()).thenReturn(parents);
	    mockMvc.perform(get("/api/parent"))
	            .andExpect(status().isOk());
	}
    
	  @Test
	    public void testGetAllParentsNotFound() throws Exception {
			List<Parent> parents = Arrays.asList(parentTest);
		    when(parentService.findAll()).thenReturn(parents);
		    mockMvc.perform(get("/api/parents"))
		            .andExpect(status().isNotFound());
		}
	    
	    public static String asJsonString(final Object obj) {
		    try {
		        final ObjectMapper mapper = new ObjectMapper();
		        final String jsonContent = mapper.writeValueAsString(obj);
		        return jsonContent;
		    } catch (Exception e) {
		        throw new RuntimeException(e);
		    }
		}  
		
		@Test
		public void testSaveParentsSuccces() throws Exception {
			when(parentService.save(parentTest)).thenReturn(parentTest);
		    mockMvc.perform(
		            post("/api/parent")
		                    .contentType(MediaType.APPLICATION_JSON)
		                    .content(asJsonString(parentTest)))
		            .andExpect(status().isCreated());
		}
		
		@Test
		public void testUpdateParentsSuccces() throws Exception {
			when(parentService.findById(parentTest.getParentId())).thenReturn(Optional.of(parentTest));
			when(parentService.save(parentTest)).thenReturn(parentTest);
		    mockMvc.perform(
		    		 put("/api/parent/{id}",1L)
		                    .contentType(MediaType.APPLICATION_JSON)
		                    .content(asJsonString(parentTest)))
		            .andExpect(status().isOk());
			when(parentService.update(parentTest)).thenReturn(parentTest);
			verify(parentService, times(1)).findById(parentTest.getParentId());
		}
		
		@Test
		public void testUpdateParentsNotFound() throws Exception {
			when(parentService.save(parentTest)).thenReturn(parentTest);
		    mockMvc.perform(
		    		 put("/api/parent/{id}",100L)
		                    .contentType(MediaType.APPLICATION_JSON)
		                    .content(asJsonString(parentTest)))
		            .andExpect(status().isNotFound());
		}
		
		@Test
		public void testDeleteParentsSuccess() throws Exception {
		    mockMvc.perform(
		            delete("/api/parent/{id}",2L))
		            .andExpect(status().isOk());
		}
		
		@Test
		public void testDeleteParentsrNotFound() throws Exception {
		    mockMvc.perform(
		            delete("/api/parent/{id}",100L))
		            .andExpect(status().isNotFound());
		}
  
	

}
