package pe.com.everis.main;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withException;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.everis.main.models.entity.Family;
import pe.com.everis.main.models.entity.Parent;
import pe.com.everis.main.restcontroller.FamilyRest;
import pe.com.everis.main.services.implement.FamilyImplement;

@RunWith(SpringRunner.class)
@WebMvcTest(FamilyRest.class)
class FamilyRestUnitTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private FamilyImplement familyService;
	
	private Family familyTest = new Family();
	
    @Before
    public void init(){
    	Parent  parentTest = new Parent(5L, "M", "Rosa", "Maria", "Savedra", "Parent");
    	familyTest= new Family(2L,parentTest,"Los Savedra");
    }
    
    
    @Test
	public void testGetAllFamiliesSucces() throws Exception {
		List<Family> families = Arrays.asList(familyTest);
	    when(familyService.findAll()).thenReturn(families);
	    mockMvc.perform(get("/api/family"))
	            .andExpect(status().isOk());
	}
    
    @Test
    public void testGetAllFamiliesNotFound() throws Exception {
		List<Family> families = Arrays.asList(familyTest);
	    when(familyService.findAll()).thenReturn(families);
	    mockMvc.perform(get("/api/familias"))
	            .andExpect(status().isNotFound());
	}
    
    public static String asJsonString(final Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        final String jsonContent = mapper.writeValueAsString(obj);
	        return jsonContent;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}  
	
	@Test
	public void testSaveFamilySuccces() throws Exception {
		when(familyService.save(familyTest)).thenReturn(familyTest);
	    mockMvc.perform(
	            post("/api/family")
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(familyTest)))
	            .andExpect(status().isCreated());
	}
	
	
	
	@Test
	public void testUpdateFamilySuccces() throws Exception {
		when(familyService.save(familyTest)).thenReturn(familyTest);
	    mockMvc.perform(
	    		 put("/api/family/{id}",1L)
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(familyTest)))
	            .andExpect(status().isOk());
	}
	
	@Test
	public void testUpdateFamilyNotFound() throws Exception {
		when(familyService.save(familyTest)).thenReturn(familyTest);
	    mockMvc.perform(
	    		 put("/api/family/{id}",100L)
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(familyTest)))
	            .andExpect(status().isNotFound());
	}
	
	@Test
	public void testDeleteFamilySuccess() throws Exception {
	    mockMvc.perform(
	            delete("/api/family/{id}",2L))
	            .andExpect(status().isOk());
	}
	
	@Test
	public void testDeleteFamilyrNotFound() throws Exception {
	    mockMvc.perform(
	            delete("/api/family/{id}",100L))
	            .andExpect(status().isNotFound());
	}
	
	
		
}
