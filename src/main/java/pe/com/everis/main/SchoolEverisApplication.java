package pe.com.everis.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolEverisApplication {

  public static void main(String[] args) {
	  
    SpringApplication.run(SchoolEverisApplication.class, args);
  }

}
