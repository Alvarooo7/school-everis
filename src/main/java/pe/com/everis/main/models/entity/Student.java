package pe.com.everis.main.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.JoinColumn;


@Entity
@Table(name = "students")
public class Student {

	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long studentId;
	  @Column(length = 1)
	  @Size(max = 1)
	  private String gender;
	  @Column(length = 20)
	  private String firstName;
	  @Column(length = 20)
	  private String middleName;
	  @Column(length = 30)
	  private String lastName;
	  @Temporal(TemporalType.DATE)
	  //@JsonFormat(pattern="yyyy/MM/dd")
	  private Date dateOfBirth;
	  @Column(length = 40)
	  private String otherStudentDetails;
	  
	  @JsonIgnore
	  @ManyToMany(mappedBy = "students")
		private List<Parent> parents;
	  
	  @JsonIgnore
	  @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
		private List<FamilyMember> familyMembers;
	  
	  public Student(Long studentId, String gender, String firstName, String middleName, 
		      String lastName,String otherStudentDetails) {
			super();
			this.studentId = studentId;
			this.gender = gender;
			this.firstName = firstName;
			this.middleName = middleName;
			this.lastName = lastName;
			this.dateOfBirth = dateOfBirth;
			this.otherStudentDetails = otherStudentDetails;
		  }
	  
	  public Student(String gender, String firstName, String middleName, 
		      String lastName, Date dateOfBirth,String otherStudentDetails) {
			super();
			this.studentId = studentId;
			this.gender = gender;
			this.firstName = firstName;
			this.middleName = middleName;
			this.lastName = lastName;
			this.dateOfBirth = dateOfBirth;
			this.otherStudentDetails = otherStudentDetails;
		  }

	  
	  public Student() {
		  parents = new ArrayList<>();
	  }
	  
	  
	  
	  public List<Parent> getParents() {
		return parents;
	}


	public void setParents(List<Parent> parents) {
		this.parents = parents;
	}


	public List<FamilyMember> getFamilyMembers() {
		return familyMembers;
	}


	public void setFamilyMembers(List<FamilyMember> familyMembers) {
		this.familyMembers = familyMembers;
	}


	public void addParent(Parent _parent) {
		  parents.add(_parent);
		}
	  
		public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getOtherStudentDetails() {
		return otherStudentDetails;
	}
	public void setOtherStudentDetails(String otherStudentDetails) {
		this.otherStudentDetails = otherStudentDetails;
	}
		
	
}
