package pe.com.everis.main.models.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.com.everis.main.models.entity.FamilyMember;

@Repository
public interface FamilyMemberRepository extends JpaRepository<FamilyMember, Long> {
	
	@Query(nativeQuery = true,value ="Select * from family_members where family_id = :idFamily")
	List<FamilyMember> fetchFamilyMembersByIdFamily(Long idFamily);	
	
}
