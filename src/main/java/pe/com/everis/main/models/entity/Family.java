package pe.com.everis.main.models.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "families")
public class Family {
	
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long familyId;
	  
	  //Nos permite mantener la persistencia
	  
	  @JoinColumn(unique = true,name = "head_of_family_parent_id")
	  @OneToOne(cascade = CascadeType.MERGE)
	  private Parent headOfFamilyParentId;
	  
	  @Column(length = 30)
	  @Size(max = 30)
	  private String familyName;
	
	  @JsonIgnore
	  @OneToMany(mappedBy = "family", fetch = FetchType.LAZY)
	  private List<FamilyMember> familyMembers;
	  
	public Family() {
		super();
	}
	public Family(Long familyId, Parent headOfFamilyParentId, String familyName) {
		super();
		this.familyId = familyId;
		this.headOfFamilyParentId = headOfFamilyParentId;
		this.familyName = familyName;
	}
	
	
	
	public List<FamilyMember> getFamilyMembers() {
		return familyMembers;
	}
	public void setFamilyMembers(List<FamilyMember> familyMembers) {
		this.familyMembers = familyMembers;
	}
	public Parent getHeadOfFamilyParentId() {
		return headOfFamilyParentId;
	}
	public void setHeadOfFamilyParentId(Parent headOfFamilyParentId) {
		this.headOfFamilyParentId = headOfFamilyParentId;
	}
	public Long getFamilyId() {
		return familyId;
	}
	public void setFamilyId(Long familyId) {
		this.familyId = familyId;
	}
	
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	 
	
}
