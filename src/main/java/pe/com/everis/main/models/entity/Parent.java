package pe.com.everis.main.models.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "parents")
public class Parent {

	 @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long parentId;
	 @Column(length = 1)
	  private String gender;
	  @Column(length = 20)
	  private String firstName;
	  @Column(length = 20)
	  private String middleName;
	  @Column(length = 30)
	  private String lastName;
	  @Column(length = 40)
	  private String otherParentDetails;
	  
	  @ManyToMany
	  @JoinTable(name = "students_parents", 
		joinColumns = @JoinColumn(columnDefinition = "student_id"),
		inverseJoinColumns = @JoinColumn(columnDefinition = "parent_cod"))
		private List<Student> students;
	  
	  @JsonIgnore
	  @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
		private List<FamilyMember> familyMembers; 
	  
	 
	  
	  
	public Parent() {
		familyMembers=new ArrayList<>();
		students =new ArrayList<>();
	}



	public Parent(Long parentId, String gender, String firstName, String middleName, String lastName,
			String otherParentDetails) {
		super();
		this.parentId = parentId;
		this.gender = gender;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.otherParentDetails = otherParentDetails;
	}
	
	
	
	public List<Student> getStudents() {
		return students;
	}



	public void setStudents(List<Student> students) {
		this.students = students;
	}



	public List<FamilyMember> getFamilyMembers() {
		return familyMembers;
	}



	public void setFamilyMembers(List<FamilyMember> familyMembers) {
		this.familyMembers = familyMembers;
	}



	public void addStudent(Student student) {
		  student.addParent(this);
		  students.add(student);
		}
	  
	  
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOtherParentDetails() {
		return otherParentDetails;
	}
	public void setOtherParentDetails(String otherParentDetails) {
		this.otherParentDetails = otherParentDetails;
	}
	
	
}
