package pe.com.everis.main.services;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import pe.com.everis.main.models.entity.Family;
import pe.com.everis.main.models.entity.FamilyMember;

public interface FamilyService  extends CrudService<Family, Long> {

}
