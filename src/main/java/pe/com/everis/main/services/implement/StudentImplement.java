package pe.com.everis.main.services.implement;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.everis.main.models.entity.Student;
import pe.com.everis.main.models.repository.StudentRepository;
import pe.com.everis.main.services.StudentService;

@Service
public class StudentImplement implements StudentService {

	@Autowired
	StudentRepository student; 
	
	
	
	public StudentImplement(StudentRepository student) {
		super();
		this.student = student;
	}

	@Transactional(readOnly = true)
	@Override
	public List<Student> findAll() throws Exception {
		// TODO Auto-generated method stub
		return student.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<Student> findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return student.findById(id);
	}

	@Transactional
	@Override
	public Student save(Student entity) throws Exception {
		// TODO Auto-generated method stub
		return student.save(entity);
	}

	@Transactional
	@Override
	public Student update(Student entity) throws Exception {
		// TODO Auto-generated method stub
		return student.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Long id) throws Exception {
		// TODO Auto-generated method stub
		student.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		// TODO Auto-generated method stub
		student.deleteAll();
	}


}
