package pe.com.everis.main.services;

import pe.com.everis.main.models.entity.Student;

public interface StudentService  extends CrudService<Student, Long> {

}
