package pe.com.everis.main.services;

import pe.com.everis.main.models.entity.Parent;

public interface ParentService extends CrudService<Parent, Long> {

}
