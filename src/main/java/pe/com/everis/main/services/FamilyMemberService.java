package pe.com.everis.main.services;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import pe.com.everis.main.models.entity.FamilyMember;

public interface FamilyMemberService extends CrudService<FamilyMember, Long> {

	
	@Query(nativeQuery = true,value ="Select * from family_members where family_id = :idFamily")
	List<FamilyMember> fetchFamilyMembersByIdFamily(Long idFamily);	
	
	
}
