package pe.com.everis.main.services.implement;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.everis.main.models.entity.Family;
import pe.com.everis.main.models.repository.FamilyRepository;
import pe.com.everis.main.services.FamilyService;

@Service
public class FamilyImplement implements FamilyService{

	@Autowired
	private FamilyRepository family;
	
	@Transactional(readOnly = true)
	@Override
	public List<Family> findAll() throws Exception {
		// TODO Auto-generated method stub
		return family.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<Family> findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return family.findById(id);
	}

	@Transactional
	@Override
	public Family save(Family entity) throws Exception {
		// TODO Auto-generated method stub
		return family.save(entity);
	}

	@Transactional
	@Override
	public Family update(Family entity) throws Exception {
		// TODO Auto-generated method stub
		return family.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Long id) throws Exception {
		// TODO Auto-generated method stub
		family.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		// TODO Auto-generated method stub
		family.deleteAll();
	}

}
