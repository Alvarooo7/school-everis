package pe.com.everis.main.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.everis.main.models.entity.Parent;
import pe.com.everis.main.services.ParentService;

@RestController
@RequestMapping("/api/parent")
public class ParentRest {

	@Autowired
	ParentService parentServ;
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< List<Parent> > fetchParent() {
		try {
			List<Parent> parents = parentServ.findAll();
			return new ResponseEntity<List<Parent>>(parents, HttpStatus.OK);   
		} catch (Exception e) {
			return new ResponseEntity<List<Parent>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< Parent > saveParent(@RequestBody Parent parent) {
		try {
			Parent newParent = parentServ.save(parent);
			return new ResponseEntity< Parent >(newParent, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity< Parent >(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< Parent > updateParent(@PathVariable("id") Long id, 
			@RequestBody Parent parent) {
		try {
			 
				Optional<Parent> optional = parentServ.findById(id);
				if(optional.isPresent()) {
					parent.setParentId(id);
					Parent updateParent = parentServ.update(parent);
					return new ResponseEntity<Parent>(updateParent, 
							HttpStatus.OK);
				} else {
					return new ResponseEntity<Parent>(HttpStatus.NOT_FOUND);
				}				
			
		} catch (Exception e) {
			return new ResponseEntity<Parent>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Parent> deleteParent(@PathVariable("id") Long id) {
		try {			
			Optional<Parent> optional = parentServ.findById(id);
			if(optional.isPresent()) {
				parentServ.deleteById(id);
				return new ResponseEntity<Parent>(HttpStatus.OK);
			} else {
				return new ResponseEntity<Parent>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<Parent>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
