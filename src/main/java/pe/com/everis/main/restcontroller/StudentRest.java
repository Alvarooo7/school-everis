package pe.com.everis.main.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.everis.main.models.entity.Student;
import pe.com.everis.main.services.StudentService;

@RestController
@RequestMapping("/api/student")
public class StudentRest {

	@Autowired
	StudentService studentServ;
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< List<Student> > fetchStudents() {
		try {
			List<Student> students = studentServ.findAll();
			return new ResponseEntity<List<Student>>(students, HttpStatus.OK);   
		} catch (Exception e) {
			return new ResponseEntity<List<Student>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< Student > saveStudent(@RequestBody Student student) {
		try {
			Student newEstudent = studentServ.save(student);
			return new ResponseEntity< Student >(newEstudent, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity< Student >(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< Student > updateStudent(@PathVariable("id") Long id, 
			@RequestBody Student student) {
		try {
			    
				Optional<Student> optional = studentServ.findById(id);
				if(optional.isPresent()) {
					student.setStudentId(id);
					Student updateStudent = studentServ.update(student);
					return new ResponseEntity<Student>(updateStudent, 
							HttpStatus.OK);
				} else {
					return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
				}				
			
		} catch (Exception e) {
			return new ResponseEntity<Student>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Student> deleteStudent(@PathVariable("id") Long id) {
		try {			
			Optional<Student> optional = studentServ.findById(id);
			if(optional.isPresent()) {
				studentServ.deleteById(id);
				return new ResponseEntity<Student>(HttpStatus.OK);
			} else {
				return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<Student>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}