package pe.com.everis.main.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.everis.main.models.entity.Family;
import pe.com.everis.main.services.FamilyService;

@RestController
@RequestMapping("/api/family")
public class FamilyRest {

	@Autowired
	FamilyService familyServ;
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< List<Family> > fetchFamily() {
		try {
			List<Family> families = familyServ.findAll();
			return new ResponseEntity<List<Family>>(families, HttpStatus.OK);   
		} catch (Exception e) {
			return new ResponseEntity<List<Family>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< Family > saveFamily( @RequestBody Family family) {
		try {
			Family newFamily = familyServ.save(family);
			
			return new ResponseEntity< Family >(newFamily, HttpStatus.CREATED);
		} catch (Exception e) {
			e.getStackTrace();
			return new ResponseEntity< Family >(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< Family > updateFamily(@PathVariable("id") Long id, 
			@RequestBody Family family) {
		try {
			
				Optional<Family> optional = familyServ.findById(id);
				if(optional.isPresent()) {
					family.setFamilyId(id);
					Family updateFamily = familyServ.update(family);
					return new ResponseEntity<Family>(updateFamily, 
							HttpStatus.OK);
				} else {
					return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
				}				
			
		} catch (Exception e) {
			return new ResponseEntity<Family>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Family> deleteFamily(@PathVariable("id") Long id) {
		try {			
			Optional<Family> optional = familyServ.findById(id);
			if(optional.isPresent()) {
				familyServ.deleteById(id);
				return new ResponseEntity<Family>(HttpStatus.OK);
			} else {
				return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<Family>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
