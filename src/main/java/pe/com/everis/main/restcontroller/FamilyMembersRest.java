package pe.com.everis.main.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import pe.com.everis.main.models.entity.FamilyMember;
import pe.com.everis.main.models.entity.Student;
import pe.com.everis.main.services.FamilyMemberService;

@RestController
@RequestMapping("/api/familymember")
public class FamilyMembersRest {
	
	@Autowired
	private FamilyMemberService familyMemberServ;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Servicio para obtener todos los miembros de todas las familias", notes = "Obtiene la información de los miembros" )
	public ResponseEntity< List<FamilyMember> > fetchFamilyMember() {
		try {
			List<FamilyMember> families = familyMemberServ.findAll();
			return new ResponseEntity<List<FamilyMember>>(families, HttpStatus.OK);   
		} catch (Exception e) {
			return new ResponseEntity<List<FamilyMember>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Servicio para guardar  los miembros de una familia" )
	public ResponseEntity< FamilyMember > saveFamilyMember(@RequestBody FamilyMember family) {
		try {
			FamilyMember newFamilyMember = familyMemberServ.save(family);
			
			return new ResponseEntity< FamilyMember >(newFamilyMember, HttpStatus.CREATED);
		} catch (Exception e) {
			e.getStackTrace();
			return new ResponseEntity< FamilyMember >(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(path = "/family/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< List<FamilyMember> > fetchFamilyMembersByIdFamily(@PathVariable("id") Long idFamily) {
		try {
			List<FamilyMember> families = familyMemberServ.fetchFamilyMembersByIdFamily(idFamily);
			return new ResponseEntity<List<FamilyMember>>(families, HttpStatus.OK);  
			
		} catch (Exception e) {
			return new ResponseEntity<List<FamilyMember>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity< FamilyMember > updateFamilyMember(@PathVariable("id") Long id, 
			@RequestBody FamilyMember family) {
		try {
			
				Optional<FamilyMember> optional = familyMemberServ.findById(id);
				if(optional.isPresent()) {
					family.setFamilyMemberId(id);
					FamilyMember updateFamilyMember = familyMemberServ.update(family);
					return new ResponseEntity<FamilyMember>(updateFamilyMember, 
							HttpStatus.OK);
				} else {
					return new ResponseEntity<FamilyMember>(HttpStatus.NOT_FOUND);
				}				
			
		} catch (Exception e) {
			return new ResponseEntity<FamilyMember>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<FamilyMember> deleteFamily(@PathVariable("id") Long id) {
		try {			
			Optional<FamilyMember> optional = familyMemberServ.findById(id);
			if(optional.isPresent()) {
				familyMemberServ.deleteById(id);
				return new ResponseEntity<FamilyMember>(HttpStatus.OK);
			} else {
				return new ResponseEntity<FamilyMember>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<FamilyMember>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	
}
